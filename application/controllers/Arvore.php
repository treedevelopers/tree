<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Arvore extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('No_model');        
        $this->load->model('/DAO/Arvore_DAO', 'Arvore_DAO');
        $this->load->model('Arvore_model');
    }

    public function newNo($valor){
        
        //Cria novo Nó
        //$valor = $this->input->post('value');
        $no = new No_model;
        $no->create($valor, 0, "a");

        var_dump($no);

        //Insere na Árvore da Memória
       /* $arvore = new Arvore_model;
        $arvore->insereNo($no);
        
        //Insere na Árvore do Banco
        $arvore_DAO = new Arvore_DAO;
        $arvore_DAO->grava_nos($arvore); */    
    }

    public function mostra(){
        $arvore = new Arvore_model;
        var_dump($arvore);
    
      /*$arvore_DAO = new Arvore_DAO;
      $arvore = $arvore_DAO->carregaArvore();
      $array = array();

      $this->geraArvore($arvore,$array);

      echo json_encode($array);*/
    }

    public function geraArvore($arvore,&$array){
        
        $no = array(
          "group" => "nodes",
          "data" => array(
              "id" => $arvore->id,
              "name" => $arvore->valor
          ),
          "position" => array(
              "x" => $arvore->x,
              "y" => $arvore->y
          )
        );

        array_push($array,$no);

        if(!is_null($arvore->esquerda)){
            $this->geraArvore($arvore->esquerda,$array);
        }

        if(!is_null($arvore->direita)){
            $this->geraArvore($arvore->direita,$array);
        }
    }
    
}
