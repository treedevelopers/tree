<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Arvore_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $arvore_DAO = new Arvore_DAO;
        $this->raiz = $arvore_DAO->carregaArvore();
    }

    private $raiz;

    public function __set($atrib, $value) {
        $this->$atrib = $value;
    }

    public function __get($atrib) {
        return $this->$atrib;
    }

    public function insereNo($no) {
        $this->percorreArvore($no, $this->raiz);
    }

    private function percorreArvore($no, &$raiz) {
        if (is_null($raiz->valor)) {
            $raiz->valor = $no->valor;
            $raiz->esquerda = new No_model;
            $raiz->direita = new No_model;
            $raiz->id = $no->id;
            $raiz->status = $no->status;
        } else {
            if ($no->valor > $raiz->valor) {
                $this->percorreArvore($no, $raiz->direita);
            } else {
                $this->percorreArvore($no, $raiz->esquerda);
            }
        }
    }

    public function altura($no) {
        if (is_null($no->esquerda) && is_null($no->direita)) {
            return 0;
        } else
        if ($this->altura($no->esquerda) > $this->altura($no->direita)) {
            return 1 + $this->altura($no->esquerda);
        } else {
            return 1 + $this->altura($no->direita);
        }
    }

    /* public function rotacionaDireita(&$no){
      $aux= $no->esquerda;
      $temp = $aux->direita;

      $aux->direita = $no;

      $no->esquerda->valor = $temp->valor;
      $no->esquerda->x     = $temp->x;
      $no->esquerda->y     = $temp->y;

      $no->valor = $aux->valor;
      $no->x     = $aux->x;
      $no->y     = $aux->y;


      $no->atualizaDados();
      $no->esquerda->atualizaDados();
      }
     */



    /*  public function rotacionaEsquerda(&$no){
      $aux = $no->direita;
      $temp = $aux->esquerda;
      $aux->esquerda = $no;
      $no->direita = $temp;
      $no = $aux;
      }

     */


    /*   public function rotaciona_esq_dir(&$no){
      $this->rotacionaEsquerda($no->esquerda);
      $this->rotacionaDireita($no);
      }
     */


    /*  public function rotaciona_dir_esq(&$no){
      $this->rotacionaDireita($no->direita);
      $this->rotacionaEsquerda($no);
      }
     */
}
