<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Arvore_DAO extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function grava_nos($arvore) {
        $dados = array(
            "nodes" => json_encode($arvore->raiz->toArray())
        );
        $this->db->insert("tree-project", $dados);
    }

    private function convertTo_No_model(&$no){   
        if(!is_null($no->id)){
            $query = $this->db->query("SELECT id,id_pai,valor, x, y FROM arvore WHERE id_pai = ".$no->id);                
            $result = $query->result();

            if (!is_null($result)){
                foreach($result as $array){                    
                    if($array->valor > $no->valor){
                        $no->direita = new No_model;
                        $no->direita->valor = $array->valor;                        
                        $no->direita->id = $array->id;
                        $no->direita->x = $array->x;
                        $no->direita->y = $array->y;
                        $this->convertTo_No_model($no->direita);
                    }
                    else{
                        $no->esquerda = new No_model;
                        $no->esquerda->valor = $array->valor;                        
                        $no->esquerda->id = $array->id;
                        $no->esquerda->x = $array->x;
                        $no->esquerda->y = $array->y; 
                        $this->convertTo_No_model($no->esquerda);
                    }                                                     
                }                           
            }else{
                return $array;
            }  
        }     
    }

    public function carregaArvore() {
        $query_raiz = $this->db->query('SELECT id, valor, x, y FROM arvore WHERE id_pai IS NULL');                
        $result_raiz = $query_raiz->result();
        
        //Nó Raiz
        $raiz = new No_model;
        $raiz->id = $result_raiz[0]->id;
        $raiz->valor = $result_raiz[0]->valor;
        $raiz->x = $result_raiz[0]->x;
        $raiz->y = $result_raiz[0]->y;
        
        //Converte Arvore para No_model
        $this->convertTo_No_model($raiz);
        
        return $raiz;
    }    
}
