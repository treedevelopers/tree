<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Graphic_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    
    private $width;
    private $height;
    
    public function __set($atrib, $value) {
        $this->$atrib = $value;
    }

    public function __get($atrib) {
        return $this->$atrib;
    }

    private function recalculaPosicao(&$no, $p, $nivel, $altura) {
        if (!is_null($no)) {
            $exp = $altura - $nivel;
            $r = 120;
            $d = pow(2, $exp) * ($r * 0.8);
            $i = ($d - $r) / 2;
            $x = $i + ($p * $d);
            $no->x = $x;
            $no->y = 100 * $nivel;
        }

        if (!is_null($no->esquerda)) {
            $this->recalculaPosicao($no->esquerda, (2 * $p), ($nivel + 1), $altura);
        }

        if (!is_null($no->direita)) {
            $this->recalculaPosicao($no->direita, (2 * $p) + 1, ($nivel + 1), $altura);
        }
    }

    /*
      public function getJSON() {
      $this->db->order_by("indice");
      $dados = $this->db->get('nodes');
      $json = array();
      foreach ($dados->result() as $nodes) {
      $array = array(
      "group" => "nodes",
      "data" => array(
      "id" => $nodes->indice,
      "name" => $nodes->valor
      ),
      "position" => array(
      "x" => intval($nodes->x),
      "y" => intval($nodes->y)
      )
      );
      array_push($json, $array);
      }
      $dados = $this->db->get('edges');
      foreach ($dados->result() as $edges) {
      if (!empty($edges->esquerda_id)) {
      $array = array(
      "group" => "edges",
      "data" => array(
      "id" => $edges->id . ".left",
      "source" => $edges->pai_id,
      "target" => $edges->esquerda_id
      )
      );
      array_push($json, $array);
      }

      if (!empty($edges->direita_id)) {
      $array = array(
      "group" => "edges",
      "data" => array(
      "id" => $edges->id . ".right",
      "source" => $edges->pai_id,
      "target" => $edges->direita_id
      )
      );
      array_push($json, $array);
      }
      }
      echo json_encode($json);
      }

      public function buscaDados() {
      $idNo = $this->input->post('id');
      $this->db->where('indice', $idNo);
      $result = $this->db->get('nodes');
      if (empty($result))
      return false;
      else {
      $dados = $result->result_array();
      $array = array(
      "status" => "<font color='#00FF00'>Balanceado</font>",
      "name" => $dados[0]["valor"],
      "indice" => $dados[0]["indice"],
      "fatorB" => "0"
      );
      echo json_encode($array);
      }
      } */
}
