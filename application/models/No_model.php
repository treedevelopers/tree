<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class No_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    private $valor;
    private $esquerda;
    private $direita;
    private $x;
    private $y;
    private $id;
    private $status;

    public function __set($atrib, $value) {
        $this->$atrib = $value;
    }

    public function __get($atrib) {
        return $this->$atrib;
    }        

    public function toArray() {
        $esquerda = NULL;
        $direita  = NULL;

        if (!is_null($this->esquerda)){
            $esquerda = $this->esquerda->toArray();            
        }

        if (!is_null($this->direita)) {
            $direita = $this->direita->toArray();            
        }
        if(!is_null($this->valor)){
            $array = array(
            "valor" => $this->valor,
            "esquerda" => $esquerda,
            "direita" => $direita,
            "x" => $this->x,
            "y" => $this->y,
            "indice" => $this->indice,
            "status" => $this->status
        );
           return $array; 
        }
        else{
            return NULL;
        }
    }

    private function altura() {
        if ((empty($this->esquerda)) && (empty($this->direita))) {
            return 0;
        } else
        if ($this->altura($this->esquerda) > $this->altura($this->direita)) {
            return 1 + $this->altura($this->esquerda);
        } else {
            return 1 + $this->altura($this->direita);
        }
    }

}
