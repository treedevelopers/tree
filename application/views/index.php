<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Project Tree</title>

  <!-- CSS  -->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?php echo base_url('assets/css/style.css'); ?>" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="<?php echo base_url('assets/css/jquery.gritter.css'); ?>" rel="stylesheet" type="text/css"  />

</head>
<body>

   <div id="barra"> 
    <button id="bt1" type="button" class="btn btn-success btn_style">
      <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add
    </button>   
  </div>
  <div class="container">
    <div id="cy"></div>
  </div>

  <div id="information" class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLiveLabel">Informações</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
         <p>
          Status: <spam id="status"></spam> <br>
          Fator de Balanceamento: <span id="fatorB"></span> <br>
          Valor: <spam id="teste"></spam> <br>
          Indice: <span id="index"></span><br>
          N&oacute; Esquerdo: <span id="left"></span><br>
          N&oacute; Direito: <span id="right"></span> <br>
          
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Fator de Balanceamento</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Altura</a>
            </li>
          </ul>
          <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab" style='border:solid;border-color:black;width:50%;height:30%;background-color:black;'>
              <p><font color='#12eb14'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</font></p>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab" style='border:solid;border-color:black;width:50%;height:30%;background-color:black;'>
             <p><font color='#12eb14'>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</font></p>

           </div>
           <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab" style='border:solid;border-color:black;width:50%;height:30%;background-color:black;'><p><font color='#12eb14'>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</font></p></div>
         </div>

       </div>
     </div>
   </div>
 </div>

 <!--  Scripts-->
 <script src="<?php echo base_url('assets/js/jquery-3.3.1.min.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/scripts.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/cytoscape.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
 <script src="<?php echo base_url('assets/js/jquery.gritter.js'); ?>"></script>

</body>
</html>
